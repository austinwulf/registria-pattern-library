/*
=== Init
 */
var gulp = require('gulp');

// plugins
var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin')
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

/*
=== JavaScript
 */

// Linting in the console!
gulp.task('lint', function(){
    return gulp.src('build/js/baseCoat.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Build baseCoat core JS
gulp.task('scripts', function(){
    return gulp.src([
        'build/js/jquery.maskedinput.js',
        'build/js/jquery.mailcheck.js',
        'build/js/jquery.frautocomplete.js',
        'build/js/baseCoat.js' ])
        .pipe(concat('baseCoat.js'))
        .pipe(gulp.dest('dist'))
        .pipe(uglify())
        .pipe(rename({extname: '.min.js'}))
        .pipe(gulp.dest('dist'));
});

/*
=== Stylesheets
 */

// Build baseCoat core CSS
gulp.task('core_styles', function(){
    return gulp.src('build/css/baseCoat.scss')
        .pipe(sass())
        .pipe(prefix('last 3 versions'))
        .pipe(gulp.dest('dist'))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist'));
});

// Build brand-specific CSS
gulp.task('brands', function(){
    return gulp.src('build/css/brands/*.scss')
        .pipe(sass())
        .pipe(prefix('last 3 versions'))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/brands'));
});

// Build portal addon CSS
gulp.task('portal_styles', function(){
    return gulp.src('build/css/portal.scss')
        .pipe(sass())
        .pipe(prefix('last 3 versions'))
        .pipe(gulp.dest('dist/portal'))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/portal'));
});

// Do all the styles tasks
gulp.task('styles', ['core_styles', 'brands', 'portal_styles']);

/*
=== Combo Tasks
 */

// Build task - minify and concat all the things!
gulp.task('build', ['styles', 'scripts']);

// Watch files for changes
gulp.task('watch', function(){
    gulp.watch('build/js/*.js', ['scripts']);
    gulp.watch('build/css/*.scss', ['styles']);
    gulp.watch('build/css/core/*.scss', ['core_styles']);
    gulp.watch('build/css/brands/*.scss', ['brands']);
    gulp.watch('build/css/portal/*.scss', ['portal_styles']);
});

// Default task
gulp.task('default', ['build', 'watch']);