/**
 * baseCoat
 * A front-end framework for Registria
 *
 * JavaScript Dependencies:
 *     jQuery v1.7 or higher
 *     Google Maps API (//maps.googleapis.com/maps/api/js)
 *
 *     Google Maps API should be included before {{ header }}
 *     with the above CDN URL. Include Google Maps API
 *     only on pages that will use the Geolocation module.
 */

// Don't be a fool, wrap your tool.
jQuery(function($){

// Undo autofocus on first field
$('input').blur();
if (window.location.href.indexOf('#') < 0) {
    window.scrollTo(0,0);
}

/*
=== Float Labels
 */

var floatLabel = {
    targetField: $('.float-label-text').next('input'),
    showLabel: function(field) {
        var label = field.closest('label').find('.float-label-text');
        label.removeClass('hidden');
    },
    hideLabel: function(field) {
        var label = field.closest('label').find('.float-label-text');
        label.addClass('hidden');
    },
    toggleFloat: function(field){
        var label = field.closest('label').find('.float-label-text'),
            fieldVal = field.val();
        if (fieldVal.length > 0){
            label.removeClass('hidden');
        } else {
            label.addClass('hidden');
        }
    }
};

$(document).on('keyup change paste', '.float-label-text+input', function(e){
    // On keyup so it happens as user types
    // On change for autocomplete
    // On paste in case they paste a value

    // Don't trigger the change if key is tab or shift
    if (e.which !== 9 && e.which !== 16) {
        floatLabel.toggleFloat($(this));
    }
});

// Unhide float label if input is prefilled
floatLabel.targetField.each(function(){
    var self = $(this);
    if ( self.val().length > 0 ) {
        floatLabel.showLabel(self);
    }
});

/*
=== Photoregister Module
 */

var photoReg = {
    targetField: $('#photo-reg-code'),
    codeLookup: function(code) {
        // Remove spaces and dashes from code
        code = code.replace('-', '');
        code = code.replace(' ', '');

        var lookupUrl = '/prc/v1/codes/'+ code +'/product';

        if (code.length > 0) {
            $.ajax({
                type: "GET",
                url: lookupUrl,
                dataType: "json",
                success: function(data, status) {
                    var model = $('#model'),
                        product = $('#product'),
                        serial = $('#serial');
                    if (serial.length < 1) serial = $('#serial_number');

                    // Populate the fields
                    model.val( data.product.sku );
                    serial.val( data.product.serial_number );
                    product.val( data.product.id );
                    
                    // Show the float labels
                    if (data.product.sku) floatLabel.showLabel(model);
                    if (data.product.serial_number) floatLabel.showLabel(serial);
                },
                error: function(error) {
                    var message = "Sorry, that code could not be found.",
                        field = photoReg.targetField;

                    // Display error message
                    if (field.next('.invalid').length < 1) {
                        field.after(
                            '<label generated="true" class="invalid">' +
                            message +
                            '</label>'
                        );
                    }
                }
            });
        }
    }
};

// Only trigger a lookup if field exists
if (photoReg.targetField.length > 0) {
    photoReg.targetField.on('blur keydown', function(e){
        var code = photoReg.targetField.val(),
            errorMsg = photoReg.targetField.next('label.invalid');

        // Reset error message
        if (errorMsg.length > 0) errorMsg.remove();

        // Pressing enter will lookup the code
        if (e.which == 13) {
            e.preventDefault();
            e.stopImmediatePropagation();
            photoReg.codeLookup(code);
        // Otherwise look up the code on blur
        } else if (e.type == 'blur') {
            photoReg.codeLookup(code);
        }
    });
}

/*
=== Geolocation
 */

var geoLocation = {
    mapsApi: $('script[src*="maps.googleapis.com/maps/api/js"]'),
    targetFields: {
        city: $('#city'),
        state: $('#state'),
        zip: $('#zip'),
        country: $('#country')
    },
    popFields: function(city, state, zip, country) {
        // populate the fields
        this.targetFields.city.val(city);
        this.targetFields.state.val(state);
        this.targetFields.zip.val(zip);
        this.targetFields.country.val(country);

        // unhide float labels
        floatLabel.showLabel(this.targetFields.city);
        floatLabel.showLabel(this.targetFields.state);
        floatLabel.showLabel(this.targetFields.zip);
        floatLabel.showLabel(this.targetFields.country);
    },
    reverseGeocode: function(fullState, location) {
        if (this.mapsApi.length > 0) {
            var geocoder = new google.maps.Geocoder(),
                settings = {'latLng': location};

            if (typeof location === 'string')
                settings = {'address': location};

            geocoder.geocode(settings, function(results){
                $.each(results, function(){
                    var result = $(this),
                        type = $.inArray(
                            'postal_code', result[0].types),
                        state;

                    if (type > -1) { // postal_code is in array
                        // get full state name if called for
                        if (fullState) {
                            $.each(result[0].address_components, function(){
                                var self = $(this),
                                    type = $.inArray(
                                        'administrative_area_level_1', self[0].types);

                                if (type > -1) {
                                    state = self[0].long_name; }
                            });
                        }

                        // get the rest of the data
                        var place = result[0].formatted_address,
                            split = place.split(', '),
                            city = split[0],
                            zip = split[1].split(' ')[1],
                            country = split[2];

                        // get state if not already filled
                        if (!state) state = split[1].split(' ')[0];

                        // normalize USA
                        if (country === 'USA') country = 'United States';
                        // platform expects uppercase country
                        // country = country.toUpperCase();

                        // Before populating the fields, check if
                        // any value is prefilled
                        $.each(geoLocation.targetFields, function(){
                            var self = $(this),
                                value = self.val(),
                                id = self.attr('id');

                            if (value !== "" &&
                                // only retain prefill for latLng
                                typeof location !== 'string') {
                                if (id == 'city') city = value;
                                if (id == 'state') state = value;
                                if (id == 'zip') zip = value;
                                if (id == 'country') country = value;
                            }
                        });

                        // populate the fields
                        geoLocation.popFields(city, state, zip, country);
                    }
                });
            });
        } else {
            // if maps API is not present, show error
            console.log('Geolocation requires the Google Maps API: //maps.googleapis.com/maps/api/js');
        }
    },
    geoLocate: function() {
        navigator.geolocation.getCurrentPosition(function(pos) {
            var lat = pos.coords.latitude,
                lng = pos.coords.longitude,
                latLng = new google.maps.LatLng(lat, lng);

            geoLocation.reverseGeocode(true, latLng);
        },
        function(err){ console.warn('ERROR('+err.code+'): '+err.message); },
        {
            enableHighAccuracy: true, // use real GPS when available
            timeout: Infinity, // don't timeout
            maximumAge: 300000 // cached locations up to 5m old
        });
    },
    geoTrigger: function(event) {
        var text = 'Would you like us to fill this information based on your current location?';
        modal.createModal(false, text, event);

        $('.geo-accept').on('click', function(){
            // This is wrapped in setTimeout due to a bug in Safari
            modal.removeModal(false);
            setTimeout(function(){
                geoLocation.geoLocate(true);
            }, 250);
        });
    }
};

// Geolocation should only trigger if a location field is present
if ( geoLocation.targetFields.city.length > 0 ||
     geoLocation.targetFields.state.length > 0 ||
     geoLocation.targetFields.zip.length > 0 ||
     geoLocation.targetFields.country.length > 0 ) {
    $('.reverse-zip').on('blur', function(){
        var zip = $(this).val();
        geoLocation.reverseGeocode(true, zip);
    });

    $('.geo-trigger').on('click', function(e){
        geoLocation.geoTrigger(e);
    });
}

/*
=== Modals
 */

var modal = {
    trigger: $('.modal-trigger'),
    createModal: function(image, text, event) {
        var modal = $(
            '<div class="modal-overlay">' +
            '<div class="modal-content">' +
            '</div></div>' );

        if (image) {
            modal.find('.modal-content')
                .prepend('<img src="'+ image +'" class="modal-image">');
        }

        if (text) {
            modal.find('.modal-content')
                .append('<p class="modal-text">'+ text +'</p>');
        }

        if ($(event.target).hasClass('geo-trigger')) {
            modal.find('.modal-content')
                .append('<div class="grid"><div class="col-1-2"><button type="button" class="geo-accept button-primary full-width">Yes please</button></div><div class="col-1-2"><button type="button" class="modal-close button-secondary full-width">No thanks</button></div></div>');
        } else {
            modal.find('.modal-content')
                .append('<button type="button" class="modal-close button-primary full-width">Close</button>');
        }

        // prevent adding additional empty modals
        if ($('.modal-content').length === 0) {
            $('body').append(modal); }
    },
    removeModal: function(event) {
        if (// Close if targets close button
            $(event.target).hasClass('modal-close') ||
            // Close if targets overlay
            $(event.target).hasClass('modal-overlay') ||
            // Close if accessed without an event
            !event ) {
            $('.modal-overlay').remove();
        }

        // Keyboard handling
        if ( event.which == 27 ) {
            // Remove modal on ESC
            $('.modal-overlay').remove();
        } else if ( event.which == 13 || event.which == 32 ) {
            // Allow space or enter if close button is :focus
            if ($('.modal-close').is(':focus')) {
                $('.modal-overlay').remove();
            }
        }
    }
};

modal.trigger.on('click', function(e){
    e.preventDefault();

    // Grab the correct image & text
    var image = $(this).attr('data-image'),
        text = $(this).attr('data-text');

    modal.createModal(image, text, e);
});

// Click-based closing for modal
$('body').on('click', '.modal-overlay', function(e){
    modal.removeModal(e);
});

// Keyboard accessibility for closing modals
$(document).on('keydown', function(e){
    if ( $('.modal-overlay').length > 0 ) { // wait until modal exists
        modal.removeModal(e);
    }
});

/*
=== Autocompletes
 */

// FrautoComplete is dependent on jQuery Validate.
// This makes sure that it works locally.
if (window.location.hostname == "localhost") {
    $('form').validate();
}

// Hook up stanard frautoComplete fields.
var acCountry = $('.autocomplete-country'),
    acStates = $('.autocomplete-state'),
    acProvinces = $('.autocomplete-province'),
    acProduct = $('.autocomplete-product'),
    acPurchasedAt = $('#purchased_at');

// Only call frautoComplete if autocomplete field exists
if (acCountry.length > 0) {
    var countries = ["United States","United Kingdom","Afghanistan","Åland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia, Plurinational State of","Bonaire, Sint Eustatius and Saba","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo, the Democratic Republic of the","Cook Islands","Costa Rica","Côte d'Ivoire","Croatia","Cuba","Curaçao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands (Malvinas)","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Holy See (Vatican City State)","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran, Islamic Republic of","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Korea, Democratic People's Republic of","Korea, Republic of","Kuwait","Kyrgyzstan","Lao People's Democratic Republic","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia, the former Yugoslav Republic of","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia, Federated States of","Moldova, Republic of","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestine, State of","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Réunion","Romania","Russian Federation","Rwanda","Saint Barthélemy","Saint Helena, Ascension and Tristan da Cunha","Saint Kitts and Nevis","Saint Lucia","Saint Martin (French part)","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Sint Maarten (Dutch part)","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syrian Arab Republic","Taiwan, Province of China","Tajikistan","Tanzania, United Republic of","Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Venezuela, Bolivarian Republic of","Viet Nam","Virgin Islands, British","Virgin Islands, U.S.","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"],
        countrySettings = {
            synonyms: {
                'United States': ['USA', 'US', 'United States of America'],
                'Canada': ['CA', 'CAN'],
                'Åland Islands': ['Aland Islands', 'Aland']
            },
            listLimit: 5
        };

    acCountry.frautoComplete(countries, countrySettings);
}

// States autocomplete
if (acStates.length > 0) {
    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California','Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii','Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana','Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota','Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire','New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota','Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island','South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

    acStates.frautoComplete(states);
}

// Provinces autocomplete
if (acProvinces.length > 0) {
    var provinces = ["Alberta","British Columbia","Manitoba","New Brunswick","Newfoundland and Labrador","Northwest Territories","Nova Scotia","Nunavut","Ontario","Prince Edward Island","Quebec","Saskatchewan","Yukon"];

    acProvinces.frautoComplete(provinces);
}

// Product autocomplete
if (acProduct.length > 0) {
    var acProductUrl = '/services/product_autocomplete.json',
        acProductParams = '';

    if (acProduct.attr('data-productcategory') !== undefined) {
        acProductUrl = '/services/product_filtered_autocomplete';
        acProductParams = { product_category_name: acProduct.attr('data-productcategory') };
    }

    $.ajax({
        url: acProductUrl,
        data: acProductParams,
        success: function(data) {
            if (data.products !== undefined) {
                data = data.products; }

            acProduct.frautoComplete(data, {
                targetField: $('#product'),
                listLimit: 5
            });
        },
        error: function(err) { throw err; }
    });
}

// Purchased At autocomplete
if (acPurchasedAt.length > 0) {
    $.ajax({
        url: '/services/organizations_autocomplete.json',
        data: {},
        success: function(data) {
            acPurchasedAt.frautoComplete(data, {
                listLimit: 5
            });
        },
        error: function(err) { throw err; }
    });
}

/*
=== Internationalization and Localization
 */

var i18n = {
    getCountry: function() {
        if ($.IPInfo !== null && $.IPInfo !== undefined) {
            return $.IPInfo.country_code;
        } else {
            return 'US';
        }
    },
    intState: function() {
        var countryCode = this.getCountry(),
            labelText = 'International State';

        if (countryCode == 'CA') {
            labelText = 'Province';
            $('#state').addClass('autocomplete-province'); }

        if (countryCode != 'US') {
            $('#state')
                .removeClass('autocomplete-state')
                .attr('name', 'user[address_attributes][international_state]')
                .parent('label').find('.float-label-text')
                .text(labelText);
        }
    },
    zipInput: function() {
        var countryCode = this.getCountry();
        if (countryCode == 'CA' || countryCode == 'GB') {
            $('#zip').attr('type', 'text');
        } else {
            $('#zip').attr('type', 'tel');
        }
    }
};

// Change State to International State
i18n.intState();

// Set postal code field type by country
// i18n.zipInput();

/*
=== Input Masks and Validation
 */

// Redefine mask definitions to allow literal 9s in masks
delete $.mask.definitions['9'];
$.mask.definitions['#'] = '[0-9]';

var inputMasks = {
    targetFields: {
        numerical: $('.mask-numerical'),
        date: $('.mask-date'),
        phone: $('.mask-phone')
    },
    maskNum: function(event, field) {
        // If keyCode isn't . (46) or a number (48 - 57), block the keypress
        if (event.which != 46 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }

        // Only allow one . in the field
        if (event.which == 46 && field.val().indexOf('.') != -1) {
            event.preventDefault();
        }
    },
    maskPhone: function(phoneVal, callback) {
        var mask,
            // Return correct mask per country
            masks = {
                1: "+1##########", // US, CA, US Minor Islands
                52: "+52#########?#", // Mexico
                44: "+44#######?###", // UK
                61: "+61#########?#", // Australia
                81: "+81#########?#", // Japan
                91: "+91##########" // India
            },
            // Country codes to match against
            countryCodes = {
                "US": 1,
                "UM": 1,
                "CA": 1,
                "GB": 44,
                "MX": 52,
                "AU": 61,
                "JP": 81,
                "IN": 91
            };

        if (phoneVal.length > 0) {
            // Check for country code in phoneVal
            $.each(masks, function(key, value){
                phoneVal = phoneVal.replace('+', '');
                if (phoneVal.indexOf(key) === 0) {
                    mask = masks[key];
                }
            });
        } else {
            // Check for country code in IPInfo
            var countryCode = i18n.getCountry(),
                code = countryCodes[countryCode];
            mask = masks[code];
        }

        if (callback) {
            callback(mask, phoneVal);
        } else {
            return { mask: mask, prefill: phoneVal };
        }
    },
    maskDate: function(field) {
        // Get country from IPInfo if available
        var country = i18n.getCountry(),
            dateFormats = {
                'mdy': ['CA', 'BZ', 'FM', 'PH', 'US'],
                'ymd': ['IR', 'SI', 'ZA', 'SE',
                        'AF', 'CN', 'HU', 'JP',
                        'KR', 'KP', 'LT', 'MN', 'TW']
                // all others are assumed dmy
                // since it is the most common
            },
            dateFormat = '';

        // Apply the correct mask
        var isMDY = ($.inArray(country, dateFormats.mdy) > -1),
            isYMD = ($.inArray(country, dateFormats.ymd) > -1);

        if (isMDY) {
            dateFormat = {
                placeholder: 'mm/dd/yyyy',
                mask: 'm#/d#/yY##',
                completed: function(field) {
                    field.attr('data-format', 'mdy');
                }
            };
        }
        if (isYMD) {
            dateFormat = {
                placeholder: 'yyyy/mm/dd',
                mask: 'yY##/m#/d#',
                completed: function(field) {
                    field.attr('data-format', 'ymd');
                }
            };
        }
        if (dateFormat === '') {
            dateFormat = {
                placeholder: 'dd/mm/yyyy',
                mask: 'd#/m#/yY##',
                completed: function(field) {
                    field.attr('data-format', 'dmy');
                }
            };
        }

        return dateFormat;
    },
    normalizeDmy: function(dateVal) {
        // Splits a date string formatted DD/MM/YYYY
        // and returns in format YYYY/MM/DD
        var dmyDate = dateVal.split('/'),
            dmyDay = dmyDate[0],
            dmyMon = dmyDate[1],
            dmyYear = dmyDate[2];

        return dmyYear +'/'+ dmyMon +'/'+ dmyDay;
    },
    valiDate: function(field) {
        // Get date and format from the field
        // and init the final return date value
        var dateVal = field.val(),
            label = field.parent('label'),
            dateFormat = field.attr('data-format'),
            finalDate = '',
            dateIsValid = true;

        // Return true if no val
        if (dateVal === '') {
            // Remove hidden field
            if (label.find('input[type="hidden"]').length > 0) {
                field
                    .addClass('registria_field')
                    .parent('label')
                    .find('input[type="hidden"]')
                    .remove();
            }

            return true;
        }

        // Normalize date value for DMY format
        // (MDY and YMD both pass into Date() correctly)
        if (dateFormat == 'dmy') {
            dateVal = this.normalizeDmy(dateVal);
        }

        // Get some date objects
        var today = new Date(),
            dateValDate = new Date(dateVal),
        // Get individual date components
            year = dateValDate.getFullYear(),
            month = dateValDate.getMonth() + 1,
            day = dateValDate.getDate();

        // Allow future dates on element
        var allowFutureDate = false;
        if (field.attr('data-allowfuturedates') == 'true') {
            allowFutureDate = true;
        }

        // Check for future dates
        if (dateValDate > today &&
            allowFutureDate == false) {
            dateIsValid = false;
        }

        // Check for invalid dates
        if (dateValDate == 'Invalid Date') {
            dateIsValid = false;
        }

        // Normalize day and month to two digits
        // and cast year as string
        if (day < 10) day = '0' + day;
        if (month < 10) month = '0' + month;
        year = year + '';

        // Reassemble the date after validations
        if (dateFormat == 'dmy') finalDate = day +'/'+ month +'/'+ year;
        if (dateFormat == 'ymd') finalDate = year +'/'+ month +'/'+ day;
        if (dateFormat == 'mdy') finalDate = month +'/'+ day +'/'+ year;

        // Update the field
        if (dateIsValid) {
            field.val(finalDate);
            // Add a hidden field with the "datepicker format"
            var datepickerDate = year +'-'+ month +'-'+ day,
                name = field.attr('name'),
                hiddenField = label.find('input[type="hidden"]');

            if (hiddenField.length === 0) {
                field
                    .removeClass('registria_field')
                    .after('<input class="registria_field valid" type="hidden" name="'+name+'" value="'+datepickerDate+'">');
            } else {
                hiddenField.val(datepickerDate);
            }
        } else {
            field
                .addClass('registria_field')
                .parent('label')
                .find('input[type="hidden"]')
                .remove();
        }

        return true;
    }
};

// Numerical-only inputs
// Add class name "mask-numerical" to the input
// Use this mask with fields that are strictly
// numerical, such as purchase price
if (inputMasks.targetFields.numerical.length > 0) {
    inputMasks.targetFields.numerical.on('keypress', function(e){
        inputMasks.maskNum(e, $(this));
    });
}

// Input mask for dates
// Add class name "mask-date" to the input
// Use this mask on fields that expect date input
// when the date is known by the user (ex. Date of Purchase)
if (inputMasks.targetFields.date.length > 0) {
    // Add mask definition for dates
    $.mask.definitions.m = '[0-1]';
    $.mask.definitions.d = '[0-3]';
    $.mask.definitions.y = '[1-2]';
    $.mask.definitions.Y = '[09]';

    var dateMask = inputMasks.maskDate();

    // Apply the mask
    inputMasks.targetFields.date.mask(dateMask.mask, {
        placeholder: dateMask.placeholder,
        completed: dateMask.completed(inputMasks.targetFields.date)
    });

    // get datepicker-style value on blur
    inputMasks.targetFields.date.on('blur', function(){
        inputMasks.valiDate($(this));
    });

    // Add rule method for future dates
    $.validator.addMethod('futureDate', function(value, element){
        // Normalize date string value if dmy
        if ($(element).attr('data-format') == 'dmy') {
            value = inputMasks.normalizeDmy(value); }

        // Allow future dates on element
        if ($(element).attr('data-allowfuturedates') == 'true') {
            return true; }

        var dateVal = new Date(value),
            today = new Date();
        if (dateVal == 'Invalid Date') {
            return true; } 
        else {
            return this.optional(element) || today > dateVal; }
    }, 'Can\'t select a future date.');

    // Add rule method for invalid dates
    $.validator.addMethod('realDate', function(value, element){
        if ($(element).attr('data-format') == 'dmy') {
            value = inputMasks.normalizeDmy(value);
        }

        var dateVal = new Date(value);
        if (value != 'mm/dd/yyyy' &&
            value != 'dd/mm/yyyy' &&
            value != 'yyyy/mm/dd' &&
            value != '') {
            return this.optional(element) || dateVal != 'Invalid Date'; }

        return true;
    }, 'Invalid date.');

    // Attach both to the field
    $.validator.addClassRules('mask-date', {
        realDate: true,
        futureDate: true,
    });

    // method to set cursor position
    $.fn.selectRange = function(start, end) {
        if (!end) end = start;
        return this.each(function(){
            if (this.setSelectionRange) {
                this.focus();
                this.setSelectionRange(start, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        });
    };

    // Make sure cursor remains in the right spot
    inputMasks.targetFields.date.on('click', function() {
        var value = $(this).val();
        if (value == 'mm/dd/yyyy' ||
            value == 'dd/mm/yyyy' ||
            value == 'yyyy/mm/dd') {
            $(this).selectRange(0);
        }
    });
}

// Input mask for phone numbers
// Add class name "mask-phone" to the input
if (inputMasks.targetFields.phone.length > 0) {
    var phoneNo = inputMasks.targetFields.phone.val();
    inputMasks.maskPhone(phoneNo, function(mask, returnVal){
        if (mask) {
            inputMasks.targetFields.phone
                .mask(mask)
                .val(returnVal)
                .blur();
        } else {
            inputMasks.targetFields.phone.mask('+1##########');
        }
    });
}

// Handle float label stuff
$.each(inputMasks.targetFields, function(){
    var targetField = $(this);

    // Float labels toggle for all visual masks
    if (! targetField.hasClass('mask-numerical')) {
        targetField.on('focus blur', function(event){
            var self = $(this);
            if (event.type == 'focus') {
                floatLabel.showLabel(self);
            } else {
                if (self.val().length < 1) {
                    floatLabel.hideLabel(self);
                }
            }
        });
    }
});

// Do mailcheck.js stuff on all email fields
$('input[type="email"]').on('blur', function(){
    var self = $(this),
        label = self.parent('label');

    self.mailcheck({
        suggested: function(element, suggestion) {
            if (label.find('.update-email').length === 0 ) {
                self.after('<p class="small-text update-email">Did you mean <span class="email-link">'+ suggestion.full +'</span>?</p>');
            }

            label.on('click', '.update-email', function(e){
                e.preventDefault();
                self.val(suggestion.full);
                label.find('.update-email').remove();
            });
        },
        empty: function(element) {
            label.find('.update-email').remove();
        }
    });
});

/*
=== Unorganized Bits
 */

// Prefill fields from URL params
var fieldPrefills = {
    fields: {
        email: $('#email'),
        phone: $('#phone')
    },
    getParam: function(param) {
        var query = window.location.search.substring(1),
            params = query.split('&');

        for (var i=0;i<params.length;i++) {
            var pair = params[i].split('=');
            if (pair[0] == param) { return pair[1]; }
        }

        return false;
    },
    popField: function(param) {
        var value = this.getParam(param);

        if (value) {
            $('#' + param).val(value);
            floatLabel.showLabel($('#' + param));
            return true;
        }

        return false;
    }
};

// Loop through fields and do the above
$.each(fieldPrefills.fields, function(key, value){
    if (fieldPrefills.getParam(key)) {
        if ($(value.selector).length > 0) {
            fieldPrefills.popField(key);
        } else {
            console.log('URL Param Prefill: Could not find field ' + value.selector);
        }
    }
});

// Custom password input stuff
var showHidePass = {
    trigger: $('.icon-eyes'),
    show: function(field) {
        var name = field.attr('name'),
            id = field.attr('id'),
            value = field.val(),
            icon = field.parent('label').find('.icon-eyes');

        icon
            .text('Hide Password')
            .removeClass('closed');
        return '<input type="text" class="registria_field" name="'+ name +'" id="'+ id +'" value="'+ value +'">';
    },
    hide: function(field) {
        var name = field.attr('name'),
            id = field.attr('id'),
            value = field.val(),
            icon = field.parent('label').find('.icon-eyes');

        icon
            .text('Show Password')
            .addClass('closed');
        return '<input type="password" class="registria_field" name="'+ name +'" id="'+ id +'" value="'+ value +'">';
    },
    toggle: function(field) {
        var type = field.attr('type'),
            newField = '';

        if (type == 'text') {
            newField = this.hide(field);
        } else if (type == 'password') {
            newField = this.show(field);
        }

        if (newField !== '') {
            field.replaceWith(newField);
        }

        return true;
    }
}

// showHidePass.toggle($('#password'));
showHidePass.trigger.on('click', function(){
    var self = $(this),
        field = $(this).parent('label').find('input');

    showHidePass.toggle(field);
});

// Custom file input stuff
$.fn.updateFilename = function() {
    this.on('change', function(){
        var label = $(this).parent('label').find('.file-label'),
            filepath = $(this).val(),
            filename = filepath.match(/[^\/\\]*$/)[0];

            if (!label.is('[data-text]')) {
                label.attr('data-text', label.text()); }

            if (filename == undefined || filename == "") {
                filename = label.attr('data-text'); }

        label.text(filename);
    });
};

var fileInput = $('label.file-input').find('input[type="file"]');
fileInput.updateFilename();

// prevent backspace in IE
$(document).on('keydown', function(event) {
    if ($('input[type="file"]:focus').length > 0) {
        if (event.which == 8) {
            event.preventDefault();
        }
    }
});

}); // end