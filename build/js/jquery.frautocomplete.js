/*  
 *  FrautoComplete - A front-end implementation of auto-complete
 *  
 *  ARGUMENTS:
 *    filter (required) - The field that will accept user input.
 *    options (optional) - An object with some settings.
 *        handlers: an object that contains custom functions to trigger after Frauto events.
 *        synonyms: an object of string keys and array values to match against
 *        listLimit: the number of choices to return on match
 *        targetField: a field to pass the value to in addition to the main target
 *
 *  
 *  USAGE EXAMPLE:
 *    Use the method on the field you want to autocomplete
 *
 *  // Standard usage
 *  var countries = ["United States", "Canada", "Mexico"]
 *  $('#country').frautoComplete(countries, {
 *    handlers: {
 *      onSelection: function() { console.log('country selected'); }
 *    },
 *    synonyms: {
 *      'United States': ['USA', 'US'],
 *      'Canada': ['CA', 'CAN'],
 *      'Mexico': ['MX']
 *    },
 *    listLimit: 5
 *  });
 *
 *  // Usage with ajax
 *  $.ajax({
 *    url: '/services/product_filtered_autocomplete',
 *    data: {},
 *    success: function(data) {
 *      var productsList = data.products;
 *      $('#product').frautoComplete(productsList);
 *    },
 *    fail: function(err) {
 *      throw err;
 *    }
 *  });
 *
 */

function FrautoComplete(filter, options) {
  // for consistent self-reference of this newly created object
  var self = this;

  // options should always be an object
  options = options || {};

  // settings are great, aren't they?
  this.handlers = options.handlers || {};
  this.synonyms = options.synonyms || {};
  this.listLimit = options.listLimit || 5;
  this.targetField = options.targetField || filter;
  this.freeInput = options.freeInput || false;

  // Require jQuery
  if (!jQuery) throw new Error('FrautoComplete requires jQuery, which was not found.');

  // Require all three expected arguments.
  if (arguments.length < 2) {
    throw new Error('FrautoComplete initialized with too few arguments');
  }
  
  // Set instance variables
  self.filter = filter;
  self.container = $(filter).parent();
  self.optionList = {};
  self.freeInput = this.freeInput;

  /* ================== *\
   *  Event listeners   *
  \* ================== */

  // prevent event listeners from getting attached twice
  $(self.filter).off();

  // mouse events
  $(self.container).on('click', '.frauto-list li', function(event) {
    self.makeSelection($(this), true);
  });
  $(self.container).on('hover', '.frauto-list li', function(event) {
    $(self.container).find('.frauto-list li.selected').removeClass('selected');
    // Removed because we have the :hover selector in CSS
    // $(this).addClass('selected');
  });

  // document events
  $(self.filter).blur(function() {
    self.onBlur();
    var value = $(self.filter).val();

    if (self.freeInput !== true) {
      self.textValidate(value); }
  });

  // handle keyboard events
  $(self.filter).on('keyup blur', function(event) {
    // FIX THIS HACK
    // patched to fix float label functionality
    // this shouldn't be necessary but the two
    // keyup events don't play together nicely
    setTimeout(function(){
      if ($(self.filter).val().length > 0) {
        $(self.filter).prev('.float-label-text').removeClass('hidden');
      } else {
        $(self.filter).prev('.float-label-text').addClass('hidden');
      }
    }, 100);

    if (event.type == 'keyup') {
      switch (event.which) {
        case 27: // escape key
          self.hideList();
          break;
        case 40: // down arrow
          self.cycle('next');
          break;
        case 38: // up arrow
          self.cycle('previous');
          break;
        default:
          self.populateList();
      }
    }
  });

  // disable enter on autocomplete fields
  $(self.filter).on('keydown', function(event){
    if (event.which === 13) {
      event.preventDefault();
    }
  });

}

FrautoComplete.prototype = {
  resetFilter: function() {
    var filter = this.filter;
    $(filter).val('');
    this.populateList();
  },

  highlightMatch: function(text, patternObj) {
    var matchBegins = text.search(patternObj);

    var matchEnds;
    if (patternObj.source != '(?:)')
      matchEnds = matchBegins+patternObj.source.length;
    else
      matchEnds = 0;

    var output = text.slice(0,matchBegins);
    output += '<span>'+ text.slice(matchBegins, matchEnds) +'</span>';
    output += text.slice(matchEnds);
    return output;
  },

  productItemTemplate: function(product, patternObj) {
    var itemText = product.product_name + ' (' + product.product_sku + ')',
        itemMarkup = '',
        dataValue = product.product_id;

    if (product.product_sku !== undefined &&
      product.product_name === product.product_sku) {
      itemText = product.product_sku; }

    if (product.id !== undefined &&
        product.id === product.value) {
      itemText = product.label + ' (' + product.sku + ')';
      dataValue = product.id;
      if (product.label === product.sku) {
        itemText = product.sku; }
    }

    if (patternObj.test(itemText)) {
      itemMarkup += '<li data-value="'+ dataValue +'">';
      itemMarkup += this.highlightMatch(itemText, patternObj) +'</li>';
    }
    return itemMarkup;
  },

  standardItemTemplate: function(item, patternObj) {
    var itemText = typeof item === 'string' ? item : item.label,
        itemId = typeof item === 'string' ? item : item.id,
        itemMarkup = '',
        max,
        i;
    if (patternObj.test(itemText)) {
      itemMarkup += '<li data-value="'+ itemId +'">';
      itemMarkup += this.highlightMatch(itemText, patternObj) +'</li>';
    } else {
      max = this.synonyms[itemText] && this.synonyms[itemText].length || 0;
      for (i=0; i<max; i++) {
        if (patternObj.test(this.synonyms[itemText][i])) {
          itemMarkup += '<li data-value="'+ itemId +'">';
          itemMarkup += itemText +'</li>';
          i = max; // kill the loop
        }
      }
    }
    return itemMarkup;
  },

  populateList: function() {
    var filter = this.filter,
        targetField = this.targetField,
        patternObj = new RegExp($(filter).val(), 'i'),
        matchList = document.createElement('ul'),
        limit = this.listLimit,
        i;

    matchList.className = 'frauto-list';

    if ($(this.targetField)[0] === $('#product')[0]) {
      for (i=0; i<this.optionList.length && $(matchList).children().length < limit; i++) {
        $(matchList).append(this.productItemTemplate(this.optionList[i], patternObj));
      }
    }
    else {
      for (i=0; i<this.optionList.length && $(matchList).children().length < limit; i++) {
        $(matchList).append(this.standardItemTemplate(this.optionList[i], patternObj));
      }
    }
    // If match list is empty (below), show "Other option"
    // but only if "other" is in the options list
    // console.log($(matchList).find('li').length);
    $('.frauto-list').remove();
    $(filter).after(matchList);
  },

  makeSelection: function(selection, hide) {
    var targetField = this.targetField;
    var filter = this.filter;
    var currentSelection = $(this.container).find('.frauto-list li.selected');
    $(currentSelection).removeClass('selected');
    if ($(targetField)[0] === $(filter)[0]) {
      $(filter).val($(selection).text()); }
    else {
      $(filter).val($(selection).text());
      $(targetField).val($(selection).attr('data-value')); }
    $(selection).addClass('selected');
    if (hide) this.hideList();

    if ($(this.filter).hasClass('invalid')) $(this.filter).valid();
    if (typeof this.handlers.onSelection !== 'undefined') {
      this.handlers.onSelection(selection);
    }
  },

  hideList: function() {
    if ($(this.container).find('.frauto-list').is(':visible'))
      $(this.container).find('.frauto-list').fadeOut('fast');
  },

  showList: function() {
    if (!$(this.container).find('.frauto-list').is(':visible'))
      $(this.container).find('.frauto-list').fadeIn('fast');
  },

  cycle: function(direction) {
    this.showList();
    var currentSelection = $(this.container).find('.frauto-list li.selected');
    if (direction === 'previous') {
      if (currentSelection.prev().length > 0)
        this.makeSelection(currentSelection.prev());
      else
        this.makeSelection($(this.container).find('.frauto-list li').last());
    }
    else {
      if (currentSelection.next().length > 0)
        this.makeSelection(currentSelection.next());
      else
        this.makeSelection($(this.container).find('.frauto-list li').first());
    }
  },

  matchString: function(string, object, targetField){
    var searchStr,
        match = false;

    if (typeof object[0] === 'string') {
      if ($.inArray(string, object) > -1) {
        match = true;
      }
    } else {
      $.each(object, function(key, value){
        if (targetField == 'product') {
          if (value.id === value.value) {
            searchStr = value.label + ' (' + value.sku + ')';
            if (value.label === value.sku) searchStr = value.sku; }
          else {
            searchStr = value.product_name + ' (' + value.product_sku + ')';
            if (value.product_name === value.product_sku) searchStr = value.product_sku; }
        }

        if (targetField == 'purchased_at') {
          searchStr = value.value; }

        if (string == searchStr) {
          match = true; }
      });
    }

    if (match === false) {
      $(this.filter).val('');
      $(this.targetField).val('');
    }
  },

  textValidate: function(text) {
    var options = this.optionList;

    if ($(this.targetField)[0] === $('#product')[0]) {
      this.matchString(text, options, 'product');
    } else if ($(this.filter)[0] === $('#purchased_at')[0]) {
      this.matchString(text, options, 'purchased_at');
    } else {
      this.matchString(text, options);
    }
  },

  onBlur: function() {
    if ($(this.container).find('.frauto-list li').length === 1) {
      this.makeSelection($(this.container).find('.frauto-list li').first(), true);
    } else {
      this.hideList();
      if ($(this.filter).val() === '') $(this.targetField).val('');
      if ($(this.targetField).val() === '') $(this.filter).val('');
    }
    if ($(this.filter).hasClass('invalid')) $(this.filter).valid();
  }
};

// Wrap the implementation in a jQuery method
jQuery.fn.frautoComplete = function(optionList, settings) {
  return this.each(function(){
    // Set defaults
    var target = $(this);
    settings = settings || { listLimit: 5 };

    var allowFreeInput = target.attr('data-allowfreeinput');

    if (allowFreeInput == "true") {
      settings.freeInput = true; }

    // Initialize the function
    var frauto = new FrautoComplete(target, settings);

    // Assign the values
    frauto.optionList = optionList;
  });
};