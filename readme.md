baseCoat
========

*A front-end framework for Registria*

## Introduction

baseCoat is a front-end framework intended to capsulize [our best practices around registration forms](https://registria.atlassian.net/wiki/display/PHREG/Registration+Form+Standards) into one codebase.

This pattern library is the easiest way to get started building pages to our new standard. It allows for rapid front-end development within our platform. We can create working mobile-optimized forms faster than ever using this foundation.

Full documentation is available at [photoregister.com/baseCoat](http://photoregister.com/basecoat).

### Getting Started
To start using the library on your project, just include baseCoat.js and baseCoat.css in your page. You can use this page as a reference to build your own form, or use the default form template for a fast and basic project.

For more control over the appearance of your page, and to add additional components to your project, it’s best to download the full repository to compile customized stylesheets and scripts. Compiling from Sass makes it much easier to make basic tweaks like changing colors, font stacks, and sizing by introducing variables to CSS.

#### Setting up your project
You’ll have to install Node, NPM, and Gulp.js on your computer to be able to compile the Sass. [Here’s a comprehensive guide for getting started.](https://travismaynard.com/writing/getting-started-with-gulp) You can use the [Sass syntax reference](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) to learn about the differences between Sass and CSS.

This repository includes everything you need to get started working with baseCoat. Pull it down into your local environment and fork it into your project folder. Then, navigate to your project folder in Terminal and run `npm install --save-dev`. This will install any dependencies Gulp requires to run tasks from gulpfile.js.

Running `gulp` in Terminal while in this directory will now start watching the directory for saved changes to the Sass partials in /build/css. Each time you save a file, all versions of baseCoat.css and baseCoat.min.css in the project folder will update with your changes.

If your Sass or JavaScript has an error, it will output in your Terminal. You can run `gulp` again to build and restart the watch process once you’ve fixed the error. You can also run `gulp lint` to output JavaScript errors with line references.

#### Branding the Template
Every implementation will require a bit of custom CSS to set brand colors and size the client's logo. In /build/css/brands, make a copy of default.scss and rename it to yourBrand.scss. Run `gulp` in your project folder, change the Sass variables to your liking, and you’ve got a new stylesheet for a new theme.

Compiled brand stylesheets appear in /dist/brands.